import { Injectable } from '@angular/core';
import {MatSnackBar} from '@angular/material';

@Injectable()
export class SnackbarService {

  constructor(
    private snackbar: MatSnackBar
  ) { }

  openSnackBar(message,action){
    this.snackbar.open(message,action);
  }

  closeSnackBar(){
    this.snackbar.dismiss();
  }
}
