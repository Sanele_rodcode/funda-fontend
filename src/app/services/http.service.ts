import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import { Global } from '../global';

@Injectable()
export class HttpService {
  headers = {headers: new HttpHeaders({'Content-Type':'application/json'})};
  constructor(
    private http: HttpClient,
    private global: Global
  ) { }

  register(user_data){
    return this.http.post(this.global.url + 'register', user_data,{headers: new HttpHeaders({'Content-Type':'application/json'})});
  }

  login(user_data){
    return this.http.post(this.global.url + 'login',user_data,this.headers);
  }

  getData(page,payload?){
    return this.http.post(this.global.url + page,payload,this.headers);
  }

  saveQuestion(question_data,user,subject,grade){
    const question_object = {
      question_data:question_data,
      user:user,
      subject:subject,
      grade:grade
    };
    return this.http.post(this.global.url + 'question/create',question_object, this.headers);
  }

  removeQuestion(question){
    return this.http.post(this.global.url + 'question/delete',{question:question}, this.headers);
  }

  updateQuestion(question){
    return this.http.post(this.global.url + 'question/update', {question:question}, this.headers);
  }
  getAnswers(question_id,user){
    return this.http.post(this.global.url + 'answers',{question_id:question_id,user:user},this.headers);
  }

  saveAnswer(answer_data){
    return this.http.post(this.global.url + 'answer/create',answer_data,this.headers);
  }

  updateAnswer(answer){
    return this.http.post(this.global.url + 'answer/update', {answer:answer}, this.headers);
  }

  removeAnswer(answer){
    return this.http.post(this.global.url + 'answer/delete',{answer:answer},this.headers);
  }

  voteAnswer(answer,user,vote){
    const vote_data = {
      answer:answer,
      user:user,
      vote:vote
    };

    return this.http.post(this.global.url + 'answer/vote', vote_data,this.headers);
  }

  uploadAvatar(file: File, user_id) {
    const data = new FormData();
    data.append('file',file);
    data.append('id',user_id);
    return this.http.post(this.global.url+'avatar/upload', data);
  }

  getTutorials(){

  }

  saveTutorial(tutorial){

  }
  getNotes(){

  }

  saveNote(note){

  }

  getCueCards(){

  }

  saveCueCards(cue_card){

  }

  updateNotifications(notifications){
    return this.http.post(this.global.url + 'notifications/update', {data:notifications}, this.headers);
  }
}
