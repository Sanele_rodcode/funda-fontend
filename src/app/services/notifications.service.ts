import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { of, Observable, BehaviorSubject } from 'rxjs';
import { Global } from '../global';

@Injectable()
export class NotificationsService {
  questionAnsweredSubject = new BehaviorSubject(null);
  private socket;
  notifications = new BehaviorSubject(null);
  incomingMessage = new BehaviorSubject(null);
  userLoggedInSubject = new BehaviorSubject(null);
  userLoggedOutSubject = new BehaviorSubject(null);;
  constructor(
    private global: Global
  ) {}

  setSocket(socket){
    this.socket = socket;

    this.socket.on('question_answered', (data) => {
      this.questionAnsweredSubject.next(data);
    });
    this.socket.on('message_sent', (data) => {
      this.incomingMessage.next(data);
    });

    this.socket.on('update_users',(data)=>{
      this.loggedIn(JSON.parse(localStorage.getItem('user')));
    });

    this.socket.on('loggedIn', (data) => {
      this.userLoggedInSubject.next(data);
    });

    this.socket.on('logout', (data)=>{
      this.userLoggedOutSubject.next(data);
    });
  }

  loggedIn(userData) {
    this.socket.emit('loggedIn',userData);
  }

  logout(user) {
    this.socket.emit('logout', user);
  }

  questionAnswered(question, answer) {
    let object = {
      'question':question,
      'answer':answer
    }
    this.socket.emit('question_answered', object);
  }

  questionAnsweredEvent(){
    return this.questionAnsweredSubject;
  }

  getNotifications(){
    return this.notifications;
  }
  
  
  setNotifications(value){
    this.notifications.next(value);
  }

  sendToUser(message){
    this.socket.emit('message_sent',message);
  }

}
