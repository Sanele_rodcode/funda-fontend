import { Injectable } from '@angular/core';
import { BehaviorSubject, from } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Global } from '../global';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  isLoggedIn$ = new BehaviorSubject(null);
  headers = {headers: new HttpHeaders({'Content-Type':'application/json'})};
  constructor(private http: HttpClient, private global: Global) { }

  set loggedIn(value){
    this.isLoggedIn$.next(value);
  }

  getLoggedInUser(){
    return JSON.parse(localStorage.getItem('user'));
  }

  getUser(userID){
    return this.http.post(this.global.url+'user', {user_id:userID}, this.headers);
  }
  
}
