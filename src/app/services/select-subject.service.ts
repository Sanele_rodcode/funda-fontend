import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class SelectSubjectService {
  selection_made;
  constructor() { }
  
  isSelectionMade(){
    return this.selection_made;
  }

  makeSelection(value){
    this.selection_made = value;
  }
}
