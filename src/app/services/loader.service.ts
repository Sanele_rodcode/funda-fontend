import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class LoaderService {
  loaderSubject = new BehaviorSubject(0);
  constructor() { }

  open(show:number){
    this.loaderSubject.next(1);
  }
  close(show:number){
    this.loaderSubject.next(0);
  }

  getLoaderSubject(){
    return this.loaderSubject;
  }
}
