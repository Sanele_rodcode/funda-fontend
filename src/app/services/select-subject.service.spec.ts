import { TestBed } from '@angular/core/testing';

import { SelectSubjectService } from './select-subject.service';

describe('SelectSubjectService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SelectSubjectService = TestBed.get(SelectSubjectService);
    expect(service).toBeTruthy();
  });
});
