import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class EditorService {
  private editorContent = new BehaviorSubject(null);

  constructor() { }

  getContent() {
    return this.editorContent;
  }

  setContent(value) {
    this.editorContent.next(value);
  }
}
