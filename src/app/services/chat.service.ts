import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as io from 'socket.io-client';
import { Global } from '../global';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ChatService {
  otherUser = new BehaviorSubject(null);
  incomingMessage = new BehaviorSubject(null);
  socket;

  headers = {headers: new HttpHeaders({'Content-Type':'application/json'})};
  
  constructor(private global: Global,private http: HttpClient) {
    //this.socket = io(this.global.url);
    // this.socket.on('message_sent',(data)=>{
    //   this.incomingMessage.next(data);
    // });
   }

  setOtherUser(user){
    this.otherUser.next(user);
  }

  getOtherUser(){
    return this.otherUser;
  }

   //chat
   sendToUser(message){
    this.socket.emit('message_sent',message);
  }

  sendToGroup(){

  }

  getChats(sender,receiver){
    return this.http.post(this.global.url+'messages',{sender_id:sender,receiver_id:receiver}, this.headers);
  }

  getContacts(user){
    return this.http.post(this.global.url+'chatroom',user,this.headers);
  }

  inviteTutor(student, tutor){
    return this.http.post(this.global.url+'tutor/invite',{student:student,tutor:tutor},this.headers);
  }
}
