import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class QuestionService {
  question$ = new BehaviorSubject(null);
  editedQuestion$ = new BehaviorSubject(null);
  editedAnswer$ = new BehaviorSubject(null);
  addedAnswer$ = new BehaviorSubject(null);
  answerToRemove$ = new BehaviorSubject(null);
  constructor() { }

  saveQuestion(question) {
    this.question$.next(question);
  }

  set question(value: any) {
    this.question$.next(value);
  }

  set editedQuestion(value: any) {
    this.editedQuestion$.next(value);
  }

  get editedQuestion() {
    return this.editedQuestion$;
  }

  set editedAnswer(value: any) {
    this.editedAnswer$.next(value);
  }

  get editedAnswer() {
    return this.editedQuestion$;
  }

  addAnswerToQuestion(question_id, answer) {
    this.addedAnswer$.next({ question_id: question_id, answer: answer });
  }

  fromAnswerFromQuestion(question_id,answer_id) {
    this.answerToRemove$.next({question_id: question_id, answer_id: answer_id});
  }
}
