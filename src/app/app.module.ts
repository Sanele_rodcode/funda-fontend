import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatButtonModule, MatDialogModule, MatExpansionModule, MatProgressSpinnerModule, MatSnackBar, MatSnackBarModule, MatMenuModule, MatIconModule} from '@angular/material';


import { AppComponent } from './app.component';
import { TopbarComponent } from './components/topbar/topbar.component';
import { BodyComponent } from './components/body/body.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { SpinnerComponent } from './components/spinner/spinner.component';
import {HttpClientModule} from '@angular/common/http';
import { ClassroomComponent } from './components/classroom/classroom.component';
import { NotesComponent } from './components/lessons/lessons.component';
import { TutorialsComponent } from './components/tutorials/tutorials.component';
import { QuestionAnswersComponent } from './components/question-answers/question-answers.component';
import { CuecardsComponent } from './components/cuecards/cuecards.component';
import {FetchDataService} from './services/fetch-data.service';
import {HttpService} from './services/http.service';
import { PopupComponent } from './components/popup/popup.component';
import {QuestionService} from './services/question.service';
import { SelectSubjectService } from './services/select-subject.service';
import { SnackbarService } from './services/snackbar.service';
import { ConfirmationPopupComponent } from './components/confirmation-popup/confirmation-popup.component';
import { EditorPopupComponent } from './components/editor-popup/editor-popup.component';
import { AccountComponent } from './components/account/account.component';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component'
import { Global } from './global';
import { FormsModule } from '@angular/forms';
import { LoaderService } from './services/loader.service';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { ChatService } from './services/chat.service';
import { NotificationsService } from './services/notifications.service';
import { EditorComponent } from './components/editor/editor.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { EditorService } from './services/editor.service';
import { SelectionComponent } from './components/selection/selection.component';
import { ChatroomComponent } from './components/chatroom/chatroom.component';
import { ChatComponent } from './components/chatroom/chat/chat.component';
import { ProfileComponent } from './components/profile/profile.component';

const appRoutes: Routes = [
  { path: '', component: BodyComponent },
  { path: 'login', component: LoginComponent  },
  { path: 'register', component: RegisterComponent },
  { path: 'classroom', component: ClassroomComponent  },
  { path: 'q-and-a', component: QuestionAnswersComponent  },
  { path: 'notifications', component: NotificationsComponent  },
  { path: 'lessons', component: NotesComponent  },
  { path: 'tutorials', component: TutorialsComponent  },
  { path: 'flash-cards', component: CuecardsComponent},
  { 
    path: 'chatroom',
    component: ChatroomComponent
  },
  { 
    path: 'chatroom/:id',
    component: ChatComponent
  },
  {
    path:'profile',
    component: ProfileComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    TopbarComponent,
    BodyComponent,
    SpinnerComponent,
    ClassroomComponent,
    NotesComponent,
    TutorialsComponent,
    QuestionAnswersComponent,
    CuecardsComponent,
    PopupComponent,
    ConfirmationPopupComponent,
    EditorPopupComponent,
    AccountComponent,
    LoginComponent,
    RegisterComponent,
    NotificationsComponent,
    EditorComponent,
    SelectionComponent,
    ChatroomComponent,
    ChatComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    MatButtonModule,
    BrowserAnimationsModule ,
    MatProgressSpinnerModule,
    HttpClientModule,
    MatExpansionModule,
    MatDialogModule,
    MatSnackBarModule,
    MatMenuModule,
    MatIconModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    CKEditorModule
  ],
  entryComponents:[
    PopupComponent,
    ConfirmationPopupComponent,
    EditorPopupComponent,
    ProfileComponent
  ],
  providers: [FetchDataService,HttpService,QuestionService,SelectSubjectService,SnackbarService, Global, LoaderService, ChatService, NotificationsService,
  EditorService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
