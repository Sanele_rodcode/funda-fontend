import {Component, ElementRef, OnInit, ViewChild, Inject, AfterViewInit} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import {HttpService} from "../../services/http.service";
import {QuestionService} from "../../services/question.service";
import { SnackbarService } from 'src/app/services/snackbar.service';
import { EditorPopupComponent } from '../editor-popup/editor-popup.component';
import { ConfirmationPopupComponent } from '../confirmation-popup/confirmation-popup.component';
import { LoaderService } from 'src/app/services/loader.service';
import { NotificationsService } from 'src/app/services/notifications.service';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent implements OnInit,AfterViewInit {

  @ViewChild('questionTopic') topic: ElementRef;
  @ViewChild('question') question: ElementRef;
  @ViewChild('answerToQuestion') answer_to_question: ElementRef;
  @ViewChild('questionLanguage') language: ElementRef;
  @ViewChild('answers') answers: ElementRef;
  message=null;
  display = true;
  
  constructor(
    public dialogRef: MatDialogRef<PopupComponent>,
    private httpService: HttpService,
    private questionService: QuestionService,
    @Inject(MAT_DIALOG_DATA) public data,
    private _snackbarService: SnackbarService,
    private popup: MatDialog,
    private loader: LoaderService,
    private notificationsService: NotificationsService
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit(){
    const likes = this.data.likes;
    if(likes){
      likes.forEach(element => {
        if(element.user_id==this.data.user.user_id && this.answers.nativeElement.querySelector('#like-btn'+element.answer_id)){
          this.answers.nativeElement.querySelector('#like-btn'+element.answer_id).classList.add('btn-success');
        }
      });
    }
  }

  closePopup(): void {
    this.dialogRef.close()
    this.display=true;
  }

  submitAnswer(question,user){
    this.loader.open(1);
    let answer = {
      user_id:user.user_id,
      username:user.first_name + ' ' + user.last_name,
      q_id:question.question_id,
      answer: this.answer_to_question.nativeElement.value
    };
    this.httpService.saveAnswer(answer).subscribe(result => {
      this.loader.close(0)
      if(result['message']=='success'){
        this.display=false;
        this.message = 'Your answer was posted. Thank you!';
        answer['answer_id'] = result['data']['insertId'];
        this.questionService.addAnswerToQuestion(question.question_id,answer);
        this.notificationsService.questionAnswered(question,answer);
      } else {
        this.message = 'Your answer was not posted. Please try again later.';
      }
    });
  }

  submitQuestion(user,subject,grade) {
    this.loader.open(1);
    const question_data = {
      topic: this.topic.nativeElement.value,
      question: this.question.nativeElement.value,
      language: this.language.nativeElement.value
    };
  
    this.httpService.saveQuestion(question_data,user,subject,grade).subscribe(result => {
      this.loader.close(0);
      if(result['message']==='success'){
        const question = {
          "question_id":result['data']['insertId'],
          "user_id":user.user_id,
          "subject":subject,
          "question":question_data.question,
          "username":user.first_name + ' ' + user.last_name,
          "grade":grade,
          "topic":question_data.topic,
          "language":question_data.language
        };
  
        this.questionService.saveQuestion(question);
        this._snackbarService.openSnackBar('Your question was saved successfully.','OK');
      }else{
        this._snackbarService.openSnackBar('Your question was not saved.','OK');
      }

    });
  }

  vote(answer,user){
    const vote = this.answers.nativeElement.querySelector('#like-btn'+answer.answer_id).classList.contains('btn-success')?0:1;
    if(answer.user_id!==user.user_id){
      this.httpService.voteAnswer(answer,user,vote).subscribe(result => {
        if(result['message']=="success"){
          this.answers.nativeElement.querySelector('#like-btn'+answer.answer_id).classList.toggle('btn-success');
          this.data.answers.forEach(element =>{
            if(element.answer_id == answer.answer_id && vote){
              element.votes+=1;
            }
  
            if(element.answer_id == answer.answer_id && !vote){
              element.votes-=1;
            }
          });
        }
      });
    }else{
      this._snackbarService.openSnackBar('You cannot like your own answer','OK');
    }
  }

  removeAnswer(answer){
    const dialogRef = this.popup.open(ConfirmationPopupComponent, {
      data:{
        popup:'warning'
      }
    });

    dialogRef.afterClosed().subscribe(confirmation => {
       if(confirmation){
        this.loader.open(1);
        this.httpService.removeAnswer(answer).subscribe(result=>{
          this.loader.close(0);
          if(result['message']==='success'){
              this._snackbarService.openSnackBar('Your answer was removed.', 'OK');
              
              this.data.answers = this.data.answers.filter(el =>{
                return el.answer_id !== answer.answer_id;
              });
              this.questionService.fromAnswerFromQuestion(answer.q_id,answer);
          }else{
            this._snackbarService.openSnackBar('Your answer was NOT deleted.', 'OK');
          }
        });
       } 
    });
  }

  editAnswer(answer){
    answer.answer = this.unescape(answer.answer);
    const dialogRef = this.popup.open(EditorPopupComponent, {
      data:{
        popup:'edit_answer',
        answer:answer
      }
    });

    dialogRef.afterClosed().subscribe(confirmation => {
      if(confirmation.value){
        this.loader.open(1);
        this.questionService.editedAnswer$.subscribe(answer => {        
          this.httpService.updateAnswer(answer).subscribe(result => {
            this.loader.close(0);
            if(result['message']=="success"){
              this._snackbarService.openSnackBar('Your answer was updated','OK');
            }
          });
        });
      }
  });
  }

  unescape(str: string){
    return unescape(str);
  }
}
