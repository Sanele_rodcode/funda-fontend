import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { EditorService } from 'src/app/services/editor.service';
import { UserService } from 'src/app/services/user.service';
@Component({
  selector: 'app-lessons',
  templateUrl: './lessons.component.html',
  styleUrls: ['./lessons.component.scss']
})
export class NotesComponent implements OnInit {

  noteToRead;
  saveCommand = false;
  editingLesson = false;
  newLesson = {
    id:null,
    title:null,
    content:null,
    created_at:null,
    status:null,
    creator:null,
    creator_id:null,
    updated_at:null,
    views:null,
    language:null,
    grade:null,
    likes:null
  }
  lessons = [
    {
      id: 1,
      author: 'Sanele',
      title: 'Testing 1',
      created_at: '2019-05-08',
      paragraphs: ["<h4>Hello World</h4>", "<p style='color:red'><span>Hey there</span></p>"]
    },
    {
      id: 2,
      author: 'Sanele',
      title: 'Testing 2',
      created_at: '2019-05-08',
      paragraphs: ["<h4>Hello World</h4>", "<p style='color:red'><span>Hey there</span></p>"]
    },
    {
      id: 3,
      author: 'Sanele',
      title: 'Testing 3',
      created_at: '2019-05-08',
      paragraphs: ["<h4>Hello World</h4>", "<p style='color:red'><span>Hey there</span></p>"]
    }
  ];
  user;
  showList = true;
  constructor(
    private sanitized: DomSanitizer,
    private userService: UserService,
    private editorService: EditorService
  ) { }

  ngOnInit() {
    this.user = this.userService.getLoggedInUser();
  }

  openNote(noteId) {
    this.showList = false;
    this.noteToRead = this.lessons.find(el => el.id === noteId);
  }

  createLesson() {
    this.saveCommand = true;
    this.editorService.getContent().subscribe(
      data => {
        if (data) {
          this.newLesson.content = data;
          this.newLesson.creator = `${this.user.first_name} ${this.user.last_name}`; 
        } else {
          
        }
      }
    );
  }

  deleteLesson() {

  }

  updateLesson() {

  }

  publishLesson() {

  }

  fetchData(selection){

  }
}
