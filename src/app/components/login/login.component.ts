import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { Router, NavigationExtras } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { LoaderService } from 'src/app/services/loader.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private _httpService: HttpService,
    private _snackbarSerivce: SnackbarService,
    private router: Router,
    private loader: LoaderService,
    private userService: UserService
  ) { }

  ngOnInit() {
  }

  login(signInForm) {
    const user_data = {
      username: signInForm.value.username,
      password: signInForm.value.password
    };

    this.loader.open(1);
    this._httpService.login(user_data).subscribe(
      result => {
        this.handleLogin(result);
      },
      error => {
        console.log(error)
      }
    );
  }

  handleLogin(result) {
    if (result.message == 'success') {
      this._snackbarSerivce.closeSnackBar();
      //load

      //loader


      const user = result['data'][0];

      window.localStorage.setItem('user', JSON.stringify(user));
      this.userService.loggedIn = user;
      this.loader.close(0);
      this.router.navigate(['/classroom']);

      // this.userService.loggedIn = user;
    } else {
      this.loader.close(0);
      this._snackbarSerivce.openSnackBar('Username and password do not match.', 'OK');
    }
  }

}
