import { Component, OnInit, Input } from '@angular/core';
import { NotificationsService } from 'src/app/services/notifications.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

  notifications;
  constructor(private notificationsService: NotificationsService) { }

  ngOnInit() {
    this.notificationsService.getNotifications().subscribe(
      notifications => {
        this.notifications = notifications;
      }
    );
  }

}
