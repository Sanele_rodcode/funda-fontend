import { Component, OnInit } from '@angular/core';
import { LoaderService } from 'src/app/services/loader.service';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit {

  show = false;
  constructor(
    private loaderService: LoaderService
  ) { }

  ngOnInit() {
    this.loaderService.getLoaderSubject().subscribe(result => {
      
      if(result===1){
        this.show = true;
      }else{
        this.show = false;
      }
    });
  }

}
