import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-selection',
  templateUrl: './selection.component.html',
  styleUrls: ['./selection.component.scss']
})
export class SelectionComponent implements OnInit {

  @Output() onFetch = new EventEmitter;
  subjects = [
    {
      id: "mathematics",
      title: 'Mathematics',
      Grade: 11,
      language: 'English',
      teacher: 'Sanele Mpangalala'
    },
    {
      id: "physics",
      title: 'Physical Science',
      Grade: 11,
      language: 'English',
      teacher: 'Sanele Mpangalala'
    },
    {
      id: "geography",
      title: 'Geography',
      Grade: 11,
      language: 'English',
      teacher: 'Sanele Mpangalala'
    },
    {
      id: "biology",
      title: 'Biology',
      Grade: 11,
      language: 'English',
      teacher: 'Sanele Mpangalala'
    }
  ];

  constructor() { }

  ngOnInit() {
    
  }

  fetch(subject:string, grade: string){
    this.onFetch.emit({"subject":subject, "grade":eval(grade)});
  }

}
