import { Component, OnInit, AfterViewInit, ViewEncapsulation, Input, OnChanges, SimpleChanges, DoCheck } from '@angular/core';
import * as CKEditor from '../../../assets/ckeditor.js';
import { EditorService } from 'src/app/services/editor.service';

declare let CKEDITOR: any;

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss'],
  encapsulation: ViewEncapsulation.None
})


export class EditorComponent implements OnInit, OnChanges {

  @Input() saveCommand;
  editor;
  constructor(
    private editorService: EditorService
  ) { }

  ngOnInit() {
    CKEDITOR.replace(document.querySelector('#editor'));
    CKEDITOR.instances.editor.getData();
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    
    if (simpleChanges.saveCommand.currentValue) {
      const data = CKEDITOR.instances.editor.getData();
      this.saveCommand = false;
    }
  }

}
