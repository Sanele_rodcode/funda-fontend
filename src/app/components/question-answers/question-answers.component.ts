import { Component, Inject, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { MatDialog } from '@angular/material';
import { PopupComponent } from '../popup/popup.component';
import { QuestionService } from 'src/app/services/question.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { EditorPopupComponent } from '../editor-popup/editor-popup.component';
import { ConfirmationPopupComponent } from '../confirmation-popup/confirmation-popup.component';
import { UserService } from 'src/app/services/user.service';
import { LoaderService } from 'src/app/services/loader.service';

@Component({
  selector: 'app-question-answers',
  templateUrl: './question-answers.component.html',
  styleUrls: ['./question-answers.component.scss']
})


export class QuestionAnswersComponent implements OnInit {
  questions;
  user;
  subject;
  grade;
  noQuestionAvailable = false;
  constructor(
    private httpService: HttpService,
    private popup: MatDialog,
    private questionService: QuestionService,
    private _snackbarService: SnackbarService,
    private userService: UserService,
    private loader: LoaderService
  ) { }

  ngOnInit() {

    this.user = this.userService.getLoggedInUser();
    this.questionService.question$.subscribe(
      question => {
        if (question) {
          let existingQuestion = this.questions.find(element => {
            element.question_id == question.question_id;
          });

          if (existingQuestion) {
          } else {
            this.questions.push(question);
          }
        }
      });

    this.questionService.addedAnswer$.subscribe(result => {
      if (result) {
        const question = this.questions.find(el => el.question_id === result.question_id);
        question['num_of_answers'] += 1;
      }
    });

    this.questionService.answerToRemove$.subscribe(result => {
      if (result) {
        const question = this.questions.find(el => el.question_id === result.question_id);
        question['num_of_answers'] -= 1;
      }
    });
  }

  openAnswerPopup(question_data) {

    const dialogRef = this.popup.open(PopupComponent, {
      data: {
        popup: 'answer',
        question: question_data,
        user: this.user
      }
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }

  openAnswersPopup(question_data) {
    this.loader.open(1);
    this.httpService.getAnswers(question_data.question_id, this.user).subscribe(result => {
      this.loader.close(0);
      const dialogRef = this.popup.open(PopupComponent, {
        data: {
          popup: 'answers',
          answers: result['data'],
          likes: result['likes'],
          user: this.user
        }
      });

      dialogRef.afterClosed().subscribe(result => {

      });
    });

  }

  openQuestionPopup() {
    const dialogRef = this.popup.open(PopupComponent, {
      data: {
        popup: 'question',
        user: this.user,
        grade: this.grade,
        subject: this.subject
      }
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }

  showDelete(question) {
    return question.user_id === this.user.user_id;
  }

  removeQuestion(question) {
    const dialogRef = this.popup.open(ConfirmationPopupComponent, {
      data: {
        popup: 'warning'
      }
    });

    dialogRef.afterClosed().subscribe(confirmation => {
      if (confirmation) {
        this.loader.open(1);
        this.httpService.removeQuestion(question).subscribe(result => {
          if (result['message'] === 'success') {
            this.loader.close(1);
            this.questions = this.questions.filter(element => {
              return element.question_id != question.question_id;
            });
          } else {
            this._snackbarService.openSnackBar('Your question was NOT deleted.', 'OK');
          }
        });
      }
    });
  }

  editQuestion(question) {
    const dialogRef = this.popup.open(EditorPopupComponent, {
      data: {
        popup: 'edit_question',
        question: question
      }
    });

    dialogRef.afterClosed().subscribe(confirmation => {
      if (confirmation.value) {
        this.loader.open(1);
        this.questionService.editedQuestion$.subscribe(question => {
          this.httpService.updateQuestion(question).subscribe(result => {
            this.loader.close(0);
            if (result['message'] == "success") {
              this.questions.forEach(element => {
                if (element.question_id === question.question_id) {
                  element = question;
                }
              });
            }
          });
        });
      }
    });
  }
  unescape(str: string) {
    return unescape(str);
  }

  fetchData(selection) {
    this.subject = selection.subject;
    this.grade = selection.grade;
    this.httpService.getData('q-and-a', selection).subscribe(
      result => {
        this.questions = result['data'];
        this.noQuestionAvailable = this.questions.length == 0 ? true : false;

      },
      error => {
        console.log(error)
      }
    );
  }
}
