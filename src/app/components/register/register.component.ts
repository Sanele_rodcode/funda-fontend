import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { Router } from '@angular/router';
import { LoaderService } from 'src/app/services/loader.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  selectSubjects = false;
  availableSubjects = [];
  userData;
  @ViewChild('signUpForm') signUpForm: ElementRef;
  constructor(
    private _httpService: HttpService,
    private _snackbarSerivce: SnackbarService,
    private router: Router,
    private loader: LoaderService) { }

  ngOnInit() {
  }

  handleRegistration(result, userData) {
    if (result['message'] == 'success') {
      let username = userData.email ? userData.email : userData.cell_number;
      this._httpService.login({ username: username, password: userData.password }).subscribe(result => {
        if (result['message'] === 'success') {
          localStorage.setItem('user', JSON.stringify(result['data'][0]));
          this.loader.close(0);
          this._snackbarSerivce.openSnackBar('You have registered successfully.', 'OK');
          setTimeout(() => {
            this._snackbarSerivce.closeSnackBar();
          }, 5000);
          this.router.navigate(['/classroom']);
        }
      });

    } else if (result['message'] === 'duplicate-email') {
      this.loader.close(0);
      this._snackbarSerivce.openSnackBar('The email is already taken', 'OK');
    } else if (result['message'] === 'duplicate-number') {
      this.loader.close(0);
      this._snackbarSerivce.openSnackBar('The cell number is already taken', 'OK');
    } else if (result['message'] === 'unexpected-error') {
      this.loader.close(0);
      this._snackbarSerivce.openSnackBar('There has been an unexpected error. Please contact <a>mpnsan005@myuct.ac.za</a> to report the issue.', 'OK');
    }
  }

  register(signUpForm) {
    const userData = {
      first_name: signUpForm.value.firstName,
      last_name: signUpForm.value.lastName,
      email: signUpForm.value.email,
      cell_number: signUpForm.value.tel,
      password: signUpForm.value.password,
      password_confirm: signUpForm.value.password_confirmation,
      user_role: signUpForm.value.role
    };

    if (userData.user_role === 'tutor') {
      this.loader.open(1);
      this._httpService.getData('subjects', null).subscribe(result => {
        this.availableSubjects = result['data'];
        this.selectSubjects = true;
        this.userData = userData;
        this.loader.close(0);
      });
    } else {
      this.loader.open(1);
      this._httpService.register(userData).subscribe(result => {
        this.handleRegistration(result, userData);
      });
    }

  }
  isValid(signUpForm) {
    return signUpForm.valid && signUpForm.value.role !== '';
  }

  continue() {
    this.loader.open(1);
    this.userData['subjects'] = this.availableSubjects;
    this._httpService.register(this.userData).subscribe(result => {
      this.handleRegistration(result, this.userData);
    });
  }
}
