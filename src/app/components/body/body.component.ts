import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {HttpService} from '../../services/http.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.scss']
})
export class BodyComponent implements OnInit {
  numberOfTutors;
  numberOfStudents;
  constructor(
    private router: Router,
    private userService: UserService,
    private httpService: HttpService
    ) { }

  ngOnInit() {
    this.httpService.getData('numbers').subscribe(
      data => {
        this.numberOfTutors = data['tutors'];
        this.numberOfStudents = data['students'];
      }
    );
    if(this.userService.getLoggedInUser()){
      this.router.navigate(['/classroom']);
    }
  }
}
