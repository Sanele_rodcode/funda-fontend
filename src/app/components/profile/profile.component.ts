import { Component, OnInit, Input } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { ChatService } from 'src/app/services/chat.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { PopupComponent } from '../popup/popup.component';
import { MatDialog } from '@angular/material';
import { Global } from 'src/app/global';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  @Input() user;
  @Input() showInviteBtn;
  subjects;
  pendingInvites = [];
  showUpload=false;
  filename;
  filesize;
  uploadBtn;
  file: File = null;

  constructor(
    private httpService: HttpService,
    private chatService: ChatService,
    private snackBar: SnackbarService,
    private userService: UserService,
    private popup: MatDialog,
    private global: Global
  ) { }

  ngOnInit() {
    if (this.user && this.user.user_role === 'tutor') {
      if (this.user && this.user.avatarUrl){
        this.user.avatarUrl = this.global.url + this.user.avatarUrl;
      }
      this.httpService.getData('tutor/subjects',{id:this.user.user_id}).subscribe(
        data => {
          this.subjects = data['data'];
        }
      );
    }

    if (!this.user){
      this.user = this.userService.getLoggedInUser();

      if (this.user && this.user.avatarUrl){
        this.user.avatarUrl = this.global.url + this.user.avatarUrl;
      }
      
      this.httpService.getData('tutor/subjects',{id:this.user.user_id}).subscribe(
        data => {
          this.subjects = data['data'];
        }
      );

      this.httpService.getData('tutor/invites',{id:this.user.user_id}).subscribe(
        data => {
          this.pendingInvites = data['data'];
        }
      );
    }
  }

  inviteTutor() {
    this.chatService.inviteTutor(this.user, this.user).subscribe(
      response => {
        this.snackBar.openSnackBar(response['message'], "OK");
      }
    );
  }

  acceptInvite(student){
    this.httpService.getData('tutor/invite/accept',{student_id:student.user_id,tutor_id:this.user.user_id}).subscribe(
      result => {
        if(result['message']==='success'){
          this.pendingInvites = this.pendingInvites.filter(el => el.user_id != student.user_id);
          this.snackBar.openSnackBar("Invite Accepted!", "OK");
        }
      }
    );
  }

  viewProfile(student) {
    const dialogRef = this.popup.open(PopupComponent, {
      data: {
        popup: 'profile',
        user: student
      }
    });
  }

  onChange(e){
    var file = <File>e.target.files[0];
    this.filename = e.path[0].files[0].name;
    this.filesize = Math.round(e.path[0].files[0].size/1024 * 100) / 100;
    this.uploadBtn = true;
    this.file = file;
  }

  uploadFiles(){
    const filename = `avatar_${this.user.user_id}.${this.filename.substr(this.filename.indexOf('.')+1)}`;
    let newFile = new File([this.file.slice(0,this.file.size+1)], filename);
    this.httpService.uploadAvatar(newFile, this.user.user_id).subscribe(
      result => {
        if (result['message']==='success') {
          this.snackBar.openSnackBar('Profile uploaded successfully','OK');
          this.user['avatarUrl'] = this.global.url+result['avatarUrl'];
          this.showUpload = false;
        }
      }
    );
  }

}
