import {Component, ElementRef, OnInit, ViewChild, Inject, AfterViewInit} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import { HttpService } from 'src/app/services/http.service';
import { QuestionService } from 'src/app/services/question.service';
import { SnackbarService } from 'src/app/services/snackbar.service';

@Component({
  selector: 'app-editor-popup',
  templateUrl: './editor-popup.component.html',
  styleUrls: ['./editor-popup.component.scss']
})
export class EditorPopupComponent implements OnInit {
  display=false;
  @ViewChild('editedQuestion') edited_question: ElementRef;
  @ViewChild('editedAnswer') edited_answer: ElementRef;
  constructor(
    public dialogRef: MatDialogRef<EditorPopupComponent>,
    private httpService: HttpService,
    private questionService: QuestionService,
    @Inject(MAT_DIALOG_DATA) public data,
    private _snackbarService: SnackbarService,
    private popup: MatDialog
  ) { }

  ngOnInit() {
  }

  closePopup(): void {
    this.dialogRef.close()
    this.display=true;
  }

  setEditedQuestion(){
    this.data.question.question = this.edited_question.nativeElement.value;
    this.questionService.editedQuestion =this.data.question;
  }

  setEditedAnswer(){
    this.data.answer.answer = this.edited_answer.nativeElement.value;
    this.questionService.editedAnswer = this.data.answer;
  }
}
