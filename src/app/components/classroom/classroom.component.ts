import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { HttpService } from '../../services/http.service';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { LoaderService } from 'src/app/services/loader.service';
import { NotificationsService } from 'src/app/services/notifications.service';
import { UserService } from 'src/app/services/user.service';
@Component({
  selector: 'app-classroom',
  templateUrl: './classroom.component.html',
  styleUrls: ['./classroom.component.scss']
})
export class ClassroomComponent implements OnInit {
  notificationsCount = 0;
  notifications;
  user;
  page = 'home';
  panelOpenState: boolean;
  message: string = null;
  instruction = 'Select a subject under "Subjects", on your left, and then select your grade. :)';


  notes = [];
  qa = [];
  tutorials = [];
  cue_cards = [];
  grade;
  subject;
  prev_subject;
  numberOfTutors;
  numberOfStudents;
  @ViewChild('subjectsContainer') subjectsContainer: ElementRef;
  @ViewChild('nav') navBar: ElementRef;
  constructor(
    private httpService: HttpService,
    private route: ActivatedRoute,
    private loader: LoaderService,
    private notificationsService: NotificationsService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.userService.isLoggedIn$.subscribe(
      user => {
        this.user = user;
      }
    );

    this.notificationsService.loggedIn(this.userService.getLoggedInUser());
    this.httpService.getData('numbers').subscribe(
      data => {
        this.numberOfTutors = data['tutors'];
        this.numberOfStudents = data['students'];
      }
    );
  }

  goTo(destination: string) {
    this.setActiveClass(destination);
    this.page = destination;

    if (this.page == 'notifications') {
      this.notifications.forEach(notification => {
        notification.is_read = 1;        
      });
      this.notificationsCount = 0;
      this.httpService.updateNotifications(this.notifications).subscribe(result => {
        //
      });
    }
  }

  setMessage(subject, grade) {
    this.loader.open(1);
    this.instruction = '';
    this.subject = subject.title;
    this.grade = grade;

    if (this.prev_subject) this.subjectsContainer.nativeElement.querySelector(`#subject-${this.prev_subject}`).classList.remove('selected-subject');
    this.subjectsContainer.nativeElement.querySelector(`#subject-${subject.id}`).classList.add('selected-subject');
    this.prev_subject = subject.id;

    let payload = {
      subject: subject.title,
      grade: grade,
    };
    this.httpService.getData(this.page, payload).subscribe(results => {
      switch (this.page) {
        case 'q-and-a':
          this.loader.close(0);
          this.qa = results['data'];
          break;

        case 'tutorials':
          this.tutorials = results['data'];
          break;

        case 'notes':
          this.notes = results['data'];
          break;

        case 'cue-cards':
          this.cue_cards = results['data'];
          break;
        default:
          break;
      }
    },
    error => {
      this.loader.close(0);
    });
  }

  setActiveClass(destination: string) {
    // get active link
    const active = this.navBar.nativeElement.querySelector('.active').classList.remove('active');
    // get clicked link
    const dest = this.navBar.nativeElement.querySelector(`#${destination}`);
    // add active class to clicked link
    dest.classList.add('active');
  }
}
