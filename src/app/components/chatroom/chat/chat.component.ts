import { Component, OnInit, AfterViewInit, AfterContentChecked, AfterViewChecked } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ChatService } from 'src/app/services/chat.service';
import { NotificationsService } from 'src/app/services/notifications.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, AfterViewChecked {

  chatMessages = [];
  user;
  otherUser;
  constructor(
    private userService: UserService,
    private chatService: ChatService,
    private notificationService: NotificationsService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.user = this.userService.getLoggedInUser();
    const user_id = parseInt(this.route.snapshot.paramMap.get("id"));
    this.chatService.getChats(this.user.user_id,user_id).subscribe(
      results => {
        this.chatMessages = results['data'];
      }
    );

    this.chatService.getOtherUser().subscribe(
      user => {
        if(user){
          this.otherUser = user;
        } else {
          this.userService.getUser(user_id).subscribe(
            data => {
              this.otherUser = data['data'];
            });
        }
      }
    );

    this.notificationService.incomingMessage.subscribe(
      message => {
        if(message){
          this.chatMessages.push(message);
        }
      }
    );
  }

  ngAfterViewChecked(){
    let element = document.querySelector(".messages div.d-flex:last-child");
    if (element){
      element.scrollIntoView();
    }
  }

  sendToUser(message){
    if (message){
      const msg = {
        sender_name:`${this.user.first_name} ${this.user.last_name}`,
        sender_id:this.user.user_id,
        receiver_id:this.otherUser.user_id,
        receiver_name: `${this.otherUser.first_name} ${this.otherUser.last_name}`,
        message:message
      }
  
      this.chatMessages.push({
        sender_name:`${this.user.first_name} ${this.user.last_name}`,
        sender_id:this.user.user_id,
        receiver_id:this.otherUser.user_id,
        receiver_name: `${this.otherUser.first_name} ${this.otherUser.last_name}`,
        message:message
      });
  
      this.notificationService.sendToUser(msg);
    }
    
  }

  sendToGroup(message){

  }

}
