import { Component, OnInit } from '@angular/core';
import { ChatService } from 'src/app/services/chat.service';
import { UserService } from 'src/app/services/user.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { HttpService } from 'src/app/services/http.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { PopupComponent } from '../popup/popup.component';
import { MatDialog } from '@angular/material';
import { NotificationsService } from 'src/app/services/notifications.service';

@Component({
  selector: 'app-chatroom',
  templateUrl: './chatroom.component.html',
  styleUrls: ['./chatroom.component.scss']
})
export class ChatroomComponent implements OnInit {

  contactList = {
    tutors: null,
    groups: null,
    students: null
  };

  user;

  tutors;
  availableTutors = [];
  searching = false;
  searchKey;
  constructor(
    private chatService: ChatService,
    private router: Router,
    private userService: UserService,
    private httpService: HttpService,
    public snackBar: SnackbarService,
    private popup: MatDialog,
    private notificationsService: NotificationsService) { }

  ngOnInit() {
    this.notificationsService.userLoggedInSubject.subscribe(
      user => {
        if (user) {
          if (this.contactList.students) {
            const student = this.contactList.students.find(student => student.user_id === user.user_id);
            if (student) {
              student.online = true;
            }
          }

          if (this.contactList.tutors) {
            const tutor = this.contactList.tutors.find(student => student.user_id === user.user_id);
            if (tutor) {
              tutor.online = true;
            }
          }
        }
      }
    );

    this.notificationsService.userLoggedOutSubject.subscribe(
      user => {
        if (user) {
          if (this.contactList.students) {
            const student = this.contactList.students.find(student => student.user_id === user.user_id);
            if (student) {
              student.online = false;
            }
          }

          if (this.contactList.tutors) {
            const tutor = this.contactList.tutors.find(student => student.user_id === user.user_id);
            if (tutor) {
              tutor.online = false;
            }
          }
        }
      }
    )

    this.user = this.userService.getLoggedInUser();
    this.chatService.getContacts(this.user).subscribe(
      data => {
        this.contactList.tutors = data['tutors'];
        this.contactList.students = data['students'];
        this.contactList.groups = data['groups'];
      }
    );
  }

  goToChat(user) {
    this.chatService.setOtherUser(user);
    this.router.navigate([`/chatroom/${user.user_id}`]);
  }

  findTutorsByName(name) {
    this.httpService.getData('tutors/findByName', { query: name }).subscribe(
      response => {
        this.availableTutors = response['data'];
      }
    );
  }

  findTutorsBySubject(subject) {
    this.httpService.getData('tutors/findBySubject', { query: subject }).subscribe(
      response => {
        this.availableTutors = response['data'];
      }
    );
  }

  listAll() {
    this.httpService.getData('tutors', null).subscribe(
      result => {
        this.availableTutors = result['data'];
      }
    );
  }

  inviteTutor(tutor) {
    this.chatService.inviteTutor(this.user, tutor).subscribe(
      response => {
        this.snackBar.openSnackBar(response['message'], "OK");
      }
    );
  }

  viewProfile(availableTutor) {
    const dialogRef = this.popup.open(PopupComponent, {
      data: {
        popup: 'profile',
        user: availableTutor
      }
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }

}
