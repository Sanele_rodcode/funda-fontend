import { Component, OnInit } from '@angular/core';
import {MatIcon} from '@angular/material/icon'
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  user;
  constructor(
    private router: Router,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.user = this.userService.getLoggedInUser();
  }

  login(){
    this.router.navigate(['/login']);
  }
  logout(){
    // delete stored user data
    localStorage.removeItem('user');
    // navigate to root
    this.router.navigate(['/login']);
  }

  editProfile(){
    
  }
}
