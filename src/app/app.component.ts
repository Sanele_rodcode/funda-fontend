import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UserService } from './services/user.service';
import { NotificationsService } from './services/notifications.service';
import { HttpService } from './services/http.service';
import * as io from 'socket.io-client';
import { Global } from './global';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'funda-frontend';
  mode = 'indeterminate';
  color = 'primary';
  page = !this.isLoggedInUser() ? "login" : "home";
  isNavOpen = false;
  notificationsCount = 0;
  notifications = [];
  @ViewChild('navbar') nav: ElementRef;
  private socket;
  constructor(
    private userService: UserService,
    private notificationsService: NotificationsService,
    private httpService: HttpService,
    private global: Global,
    private router: Router
  ) {
    this.notificationsService.setSocket(io(this.global.url));
  }

  isLoggedInUser() {
    return this.userService.getLoggedInUser() ? true : false;
  }

  getUser() {
    return this.userService.getLoggedInUser();
  }

  ngOnInit() {
    this.notificationsService.questionAnsweredEvent().subscribe(result => {
      if (result) {
        if (this.page !== 'notifications') {
          this.notificationsCount += 1;
        }
        this.notifications.push(result);
      }
    });

    this.userService.isLoggedIn$.subscribe(
      user => {
        if (user || this.userService.getLoggedInUser()) {
          this.httpService.getData('notifications', { "user": user ? user : this.userService.getLoggedInUser() }).subscribe(result => {
            this.notificationsService.setNotifications(result['data']);
          }); 
        }
      }
    );

    this.notificationsService.getNotifications().subscribe(
      notifications => {
        if (notifications) {
          this.notifications = notifications;
          this.notificationsCount = this.notifications.filter(el => el.is_read === 0).length;
        }
      }
    );

  }

  markNotificationsAsRead() {
    this.notifications.forEach(notification => {
      notification.is_read = 1;
    });
    this.notificationsCount = 0;
    this.httpService.updateNotifications(this.notifications).subscribe(result => {
      //
    });
  }

  openNav() {
    this.isNavOpen = true;
  }

  closeNav() {
    this.isNavOpen = false;
  }

  logout(){
    this.notificationsService.logout(this.userService.getLoggedInUser());
    localStorage.clear();
    this.router.navigate(["classroom"]);
  }
}
